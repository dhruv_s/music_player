package dhruvhacks.music_player;

        import android.media.MediaPlayer;
        import android.os.Bundle;
        import android.support.v7.app.ActionBarActivity;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;

class MainActivity extends ActionBarActivity {


    Button b1, b2, b3;
    MediaPlayer mp;
    boolean playing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getInit();
        getWorkdone();
    }

    private void getWorkdone() {

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!playing){
                    mp.start();
                    playing = true;
                }
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playing = true){

                    mp.pause();

                    playing = false;
                }
            }
        });


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playing = true){

                    mp.stop();
                    playing = false;


                }
                MainActivity.this.finish();
            }
        });

    }

    private void getInit() {

        b1 = (Button) findViewById(R.id.play_button);
        b2 = (Button) findViewById(R.id.pause_button);
        b3 = (Button) findViewById(R.id.stop_button);
        mp = MediaPlayer.create(this, R.raw.a);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
